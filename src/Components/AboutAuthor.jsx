import React from 'react'
import Navbar from './Navbar'
import './css/Aboutauthor.css'


const AboutAuthor = () => {
  return (
    <div className='nav-sec'>
    <Navbar/>
    <div className='about-split left'>
        <div className='about-centered'>
          <div className='about-img-main'>
          <img src='author.jpg' alt='img'></img>
          </div>
          <h4>PHILIP PULLMAN</h4>
        </div>
        </div>
        <div className='about-split right' style={{display: "flex", justifyContent: 'center'}}>
          <div className='about-p'>
          <h1>ABOUT THE AUTHOR</h1>
          <p>Philip Pullman, in full Philip Nicholas Pullman, (born October 19, 1946, Norwich, England), British author of novels for children and young adults who is best known for the fantasy trilogy His Dark Materials (1995–2000).</p>
          <p>The literary world Pullman created was inspired by his regular walks along the rivers and canals of Oxford, and “looking at maps of the city, which is laced through and through with water”.</p>
          </div>
        </div>
    </div>
  )
}

export default AboutAuthor