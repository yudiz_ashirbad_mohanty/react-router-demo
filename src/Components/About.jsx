import React from 'react'
import Navbar from './Navbar'
import './css/Aboutbook.css'
const About = () => {
  return (
    <div>
    <Navbar/>
    <div>
       <div className='book-split left'>
        <div className='book-centered'>
          <div className='book-img-main'>
          <img src='book.png' alt='img'></img>
          </div>
         
        </div>
        </div>
        <div className='book-split right' style={{display: "flex", justifyContent: 'center'}}>
          <div className='book-p'>
          <h1>ABOUT THE BOOK</h1>
          <p>The Book of Dust is a trilogy of fantasy novels by Philip Pullman, which expands his trilogy His Dark Materials. The books further chronicle the adventures of Lyra Belacqua and her battle against the theocratic organisation known as the Magisterium, and shed more light on the mysterious substance called Dust.The first book, La Belle Sauvage, was published in October 2017, and is set 12 years before Northern Lights. It describes how the 11-year-old Malcolm Polstead and the 15-year-old Alice protect the infant Lyra; leading to her being in the care of Jordan College.</p>
          </div>
        </div>
    </div>
    </div>
    
  )
}

export default About