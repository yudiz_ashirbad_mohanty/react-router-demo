import React from 'react'
import Navbar from './Navbar'
import './css/Home.css'

const Home = () => {
  return (
    <div>
        <Navbar/>
        <div className='split left'>
        <div className='centered'>
          <div className='img-main'>
          <img src='book.jpg' alt='img'></img>
          </div>
          <h3>PHILIP PULLMAN</h3>
        </div>
        </div>
        <div className='split right'>
          <div className='centered'>
          <h3>" THE BOOK OF DUST ",PHILIP PULLMAN</h3>
          <p>Winner of 6 book awards,Astrid Lindgren Memorial Award,CILIP Carnegie Medal</p>
          </div>
        </div>
    </div>
  )
}

export default Home