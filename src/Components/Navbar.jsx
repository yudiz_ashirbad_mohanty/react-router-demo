import React from 'react'
import './css/Navbar.css'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
const Navbar = () => {
  let history =useHistory();
  const onlogout =()=>{
    localStorage.removeItem('token','value')
    history.push('/')
  }
  return (
    <div className='nav'>
    <ul>
        <li> <Link to='/home'>HOME</Link></li>
        <li><Link to='/aboutauthor'>AUTHOR</Link></li>
        <li><Link to='/about'>ABOUT</Link></li>
        <li><Link to='/booklist'>BOOKLIST</Link></li>
        <div>
        <button onClick={onlogout}>logout</button>
        </div>
    </ul>
     
    </div>
  )
}

export default Navbar