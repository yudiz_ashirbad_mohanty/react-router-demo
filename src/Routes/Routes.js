import React from 'react'
import { Route } from 'react-router-dom'
import { Switch } from 'react-router-dom'

import Error from '../Components/Error'
import Login from '../Components/Login'
import User from '../Components/User'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import Home from '../Components/Home'
import AboutAuthor from '../Components/AboutAuthor'
import About from '../Components/About'
import Booklist from '../Components/Booklist'
const Routes = (props) => {
  return (
    <div>
      <Switch>
        <PublicRoute  path='/' exact component={Login}/>
        <PrivateRoute path='/home' exact component={Home}/>
        <PrivateRoute path='/aboutauthor' exact component={AboutAuthor}/>
        <PrivateRoute path='/about' exact component={About}/>
        <PrivateRoute path='/booklist' exact component={Booklist}/>
        <PrivateRoute path='/user/:name' component={User}/>
        <Route component={Error}/>
      </Switch>
    </div>
  )
}

export default Routes